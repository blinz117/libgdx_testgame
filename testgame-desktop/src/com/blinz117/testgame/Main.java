package com.blinz117.testgame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "testgame";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 480;
		cfg.resizable = false;
		
		new LwjglApplication(new TestGame(), cfg);
	}
}
