package com.blinz117.testgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TestGame extends Game {

   SpriteBatch batch;
   OrthographicCamera camera; 
   BitmapFont font;
   
   // CONSTANTS
   final int VIEW_WIDTH = 1200;
   final int VIEW_HEIGHT = 720;
   
   final float FONT_SCALE = 2.0f;

   
	public void create() {
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, VIEW_WIDTH, VIEW_HEIGHT);
		batch = new SpriteBatch();
		//Use LibGDX's default Arial font.
		font = new BitmapFont();
		font.setScale(FONT_SCALE);
		this.setScreen(new WelcomeScreen(this));
	}

	public void render() {
		super.render(); //important!
	}
	
	public void dispose() {
		batch.dispose();
		font.dispose();
	}
}