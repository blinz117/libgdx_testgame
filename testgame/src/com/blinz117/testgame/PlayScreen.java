package com.blinz117.testgame;

import java.util.Iterator;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class PlayScreen implements Screen {

	final TestGame game;

	Texture dropImage;
	Texture bucketImage;
	Rectangle bucket;
	Array<Rectangle> raindrops;
	long lastDropTime;
	Music rockMusic;
	boolean hasAccel;
	boolean isZoomed;
	
	int dropsGathered;
	
	//Constants:
	final int BUCKET_SPEED = 350;
	final int BUCKET_TILT_SPEED = 200;
	final int RAIN_SPEED = 200;
	
	final int RAIN_HEIGHT = 64;
	final int RAIN_WIDTH = 64;
	final int BUCKET_HEIGHT = 64;
	final int BUCKET_WIDTH = 64;
	

	public PlayScreen(final TestGame gam) {
		game = gam;

		dropsGathered = 0;
		// load the images for the droplet and the bucket, 64x64 pixels each
		dropImage = new Texture(Gdx.files.internal("droplet.png"));
		bucketImage = new Texture(Gdx.files.internal("bucket.png"));

		rockMusic = Gdx.audio.newMusic(Gdx.files.internal("upbeat_rock_loop.wav"));

		// start the playback of the background music immediately
		rockMusic.setLooping(true);
		//rockMusic.play();		
		
		hasAccel = Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer);
		
		// create a Rectangle to logically represent the bucket
		bucket = new Rectangle();
		bucket.x = game.VIEW_WIDTH / 2 - BUCKET_WIDTH / 2; // center the bucket horizontally
		bucket.y = 20; // bottom left corner of the bucket is 20 pixels above
						// the bottom screen edge
		bucket.width = BUCKET_WIDTH;
		bucket.height = BUCKET_HEIGHT;

		isZoomed = false;

		// create the raindrops array and spawn the first raindrop
		raindrops = new Array<Rectangle>();
		spawnRaindrop();
	}

	private void spawnRaindrop() {
		Rectangle raindrop = new Rectangle();
		raindrop.x = MathUtils.random(0, game.VIEW_WIDTH - RAIN_WIDTH);
		raindrop.y = game.VIEW_HEIGHT;
		raindrop.width = RAIN_WIDTH;
		raindrop.height = RAIN_HEIGHT;
		raindrops.add(raindrop);
		lastDropTime = TimeUtils.nanoTime();
	}

	@Override
	public void render(float delta) {
		updateWorld();
		drawWorld();
	}
	
	private void updateWorld()
	{
		//process user input
		if (hasAccel) 
		{
			if (Math.abs(Gdx.input.getAccelerometerY()) > 0.5)
				bucket.x += Gdx.input.getAccelerometerY() * BUCKET_TILT_SPEED
						* Gdx.graphics.getDeltaTime();
		} 
		else if (Gdx.input.isTouched()) 
		{
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			game.camera.unproject(touchPos);
			bucket.x = touchPos.x - BUCKET_WIDTH / 2;
		} 
		else 
		{
			if (Gdx.input.isKeyPressed(Keys.LEFT))
				bucket.x -= BUCKET_SPEED * Gdx.graphics.getDeltaTime();
			if (Gdx.input.isKeyPressed(Keys.RIGHT))
				bucket.x += BUCKET_SPEED * Gdx.graphics.getDeltaTime();
		}
//		if (Gdx.input.isTouched() && !isZoomed) {
//			game.camera.zoom = game.camera.zoom * 2;
//			isZoomed = true;
//		} else if (!Gdx.input.isTouched() && isZoomed) {
//			game.camera.zoom = game.camera.zoom / 2;
//			isZoomed = false;
//		}

		// make sure the bucket stays within the screen bounds
		if (bucket.x < 0)
			bucket.x = 0;
		if (bucket.x > game.VIEW_WIDTH - BUCKET_WIDTH)
			bucket.x = game.VIEW_WIDTH - BUCKET_WIDTH;

		// check if we need to create a new raindrop
		if (TimeUtils.nanoTime() - lastDropTime > 1000000000)
			spawnRaindrop();

		// move the raindrops, remove any that are beneath the bottom edge of
		// the screen or that hit the bucket. In the later case we play back
		// a sound effect as well.
		Iterator<Rectangle> iter = raindrops.iterator();
		while (iter.hasNext()) {
			Rectangle raindrop = iter.next();
			raindrop.y -= RAIN_SPEED * Gdx.graphics.getDeltaTime();
			if (raindrop.y + RAIN_HEIGHT < 0)
				iter.remove();
			if (raindrop.overlaps(bucket)) {
				dropsGathered++;
				iter.remove();
			}
		}
	}
	
	private void drawWorld()
	{
		// clear the screen with a dark blue color. The
		// arguments to glClearColor are the red, green
		// blue and alpha component in the range [0,1]
		// of the color to be used to clear the screen.
		Gdx.gl.glClearColor(0.05f, 0.45f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		// tell the camera to update its matrices.
		game.camera.update();

		// tell the SpriteBatch to render in the
		// coordinate system specified by the camera.
		game.batch.setProjectionMatrix(game.camera.combined);

		// begin a new batch and draw the bucket and
		// all drops
		game.batch.begin();
		game.font.draw(game.batch, "Drops Collected: " + dropsGathered, 0, game.VIEW_HEIGHT);
		game.batch.draw(bucketImage, bucket.x, bucket.y);
		for (Rectangle raindrop : raindrops) {
			game.batch.draw(dropImage, raindrop.x, raindrop.y);
		}
		if (hasAccel) {
			game.font.draw(game.batch,
					"Accel x: " + Gdx.input.getAccelerometerX(), 10, 30);
			game.font.draw(game.batch,
					"Accel y: " + Gdx.input.getAccelerometerY(), 10, 15);
		} else
			game.font.draw(game.batch, "No accelerometer", 10, 15);

		//game.font.draw(game.batch, "Zoom: " + game.camera.zoom, 10, 45);
		game.batch.end();
	}

	@Override
	public void dispose() {
		// dispose of all the native resources
		dropImage.dispose();
		bucketImage.dispose();
		game.batch.dispose();
		rockMusic.dispose();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

}
